import org.apache.log4j.Logger;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Set;

public abstract class FileWriterClass {
    private String pathname;

    public FileWriterClass(String pathname) {
        //Constructors shouldn't call overridables or send this
        this.pathname = pathname;
    }

    abstract StringBuilder getFormat(Set<Person> personSet);

    public void writeFile(Set<Person> personSet) throws IOException {
        Logger log = Logger.getLogger(this.getClass());
        StringBuilder stringBuilder = getFormat(personSet);

        File file = new File(pathname);
        log.info("file exist " + file.exists());
        if (file.exists()){
            log.info("create file " + file.createNewFile());

        }
        PrintWriter printWriter = new PrintWriter(file);
        printWriter.printf(stringBuilder.toString());
        printWriter.flush(); //guarantees that buffer data is written to destination
        printWriter.close(); //close stream ( and file )
    }
}



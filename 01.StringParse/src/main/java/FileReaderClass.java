import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import java.io.*;
import java.text.ParseException;
import java.util.Set;
import java.util.TreeSet;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public abstract class FileReaderClass {
    private String pathname;

    public FileReaderClass(String pathname) {
        this.pathname = pathname;
    }


    abstract Set<Person> getMatcher(File file) throws IOException, ParseException, SAXException, ParserConfigurationException;


    public Set<Person> readFile() throws IOException, ParseException, ParserConfigurationException, SAXException {
        Set<Person> personSet = null;

        File file = new File(pathname);
        if(file.exists())
        {
            personSet = getMatcher(file);

        }else{
            throw new FileNotFoundException();
        }
        return personSet;
    }
}

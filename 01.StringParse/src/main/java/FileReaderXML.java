import org.apache.log4j.Logger;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.text.ParseException;
import java.util.Set;
import java.util.TreeSet;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class FileReaderXML extends FileReaderClass {
    public FileReaderXML(String pathname) {
        super(pathname);
    }
    Set<Person>  getMatcher(File file) throws IOException, ParseException, SAXException, ParserConfigurationException {
        Set<Person> personSet = new TreeSet<Person>();

        DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
        Document doc = dBuilder.parse(file);

        doc.getDocumentElement().normalize();

        NodeList nList = doc.getElementsByTagName("person");

        for (int temp = 0; temp < nList.getLength(); temp++) {

            Node nNode = nList.item(temp);

            if (nNode.getNodeType() == Node.ELEMENT_NODE) {

                Element eElement = (Element) nNode;

                Person person = new Person();

                person.setName(eElement
                        .getElementsByTagName("name")
                        .item(0)
                        .getTextContent());
                person.setSurname(eElement
                        .getElementsByTagName("surname")
                        .item(0)
                        .getTextContent());
                person.setGender(eElement
                        .getElementsByTagName("gender")
                        .item(0)
                        .getTextContent().charAt(0));
                person.setAge(Integer.parseInt(eElement
                        .getElementsByTagName("age")
                        .item(0)
                        .getTextContent()));

                person.setDate(DateFormatClass.getDateObj(eElement
                        .getElementsByTagName("date")
                        .item(0)
                        .getTextContent()));
                person.setAdress(eElement
                        .getElementsByTagName("address")
                        .item(0)
                        .getTextContent());

                //@mention
                //TODO: check daca atributes din person !null ??


                personSet.add(person);

            }
        }

        return personSet;
    }


}

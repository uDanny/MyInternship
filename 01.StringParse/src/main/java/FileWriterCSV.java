import java.util.Set;

public class FileWriterCSV extends FileWriterClass{


    public FileWriterCSV(String pathname) {
        super(pathname);
    }

    StringBuilder getFormat(Set<Person> personSet) {
        StringBuilder stringBuilder = new StringBuilder();
        for (Person person : personSet) {
            stringBuilder.append(String.format("%1$s, %2$s, %3$c, %4$d, %5$s, \"%6$s\" ",person.getName(), person.getSurname(), person.getGender(), person.getAge(), DateFormatClass.getDateStr(person.getDate()), person.getAdress()));
            stringBuilder.append("\n");
        }
        return stringBuilder;
    }
}
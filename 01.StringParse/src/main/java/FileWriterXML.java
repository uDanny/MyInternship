import java.util.Set;

public class FileWriterXML extends FileWriterClass{


    public FileWriterXML(String pathname) {
        super(pathname);
    }

    StringBuilder getFormat(Set<Person> personSet) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("<?xml version=\"1.0\"?>\n");
        stringBuilder.append("<Persons>\n");
        for (Person person : personSet) {
            stringBuilder.append("\t<person>\n");
            stringBuilder.append(String.format(
                    "\t\t<name>%1$s</name>\n" +
                    "\t\t<surname>%2$s</surname>\n" +
                    "\t\t<gender>%3$c</gender>\n" +
                    "\t\t<age>%4$d</age>\n" +
                    "\t\t<date>%5$s</date>\n" +
                    "\t\t<address>\"%6$s\"</address>\n",
                    person.getName(), person.getSurname(), person.getGender(), person.getAge(), DateFormatClass.getDateStr(person.getDate()), person.getAdress()));
            stringBuilder.append("\t</person>\n");
        }
        stringBuilder.append("</Persons>");
        return stringBuilder;
    }
}
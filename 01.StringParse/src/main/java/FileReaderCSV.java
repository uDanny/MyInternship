import org.apache.log4j.*;

import java.io.*;
import java.text.ParseException;
import java.util.Set;
import java.util.TreeSet;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class FileReaderCSV extends FileReaderClass {
    static final public String regex = "([A-Za-z]+), ([A-Za-z]+), ([M,F]), (\\d{1,3}), ([0-2][0-9]|[3][0-1])-([0][0-9]|[1][0-2])-(\\d{1,4}) ([0,1][0-9]|[2][0-4]):([0-6][0-9]), \"(.+)\"";

    public FileReaderCSV(String pathname) {
        super(pathname);
    }

    Set<Person>  getMatcher(File file) throws IOException, ParseException {


        Set<Person> personSet = new TreeSet<Person>();
        FileReader fileReader = new FileReader(file);
        BufferedReader bufferedReader = new BufferedReader(fileReader);

        Pattern pattern = Pattern.compile(regex);

        String sCurrentLine;
        while ((sCurrentLine = bufferedReader.readLine()) != null) {
            Matcher matcher = pattern.matcher(sCurrentLine);

            while (matcher.find()) {
                personSet.add(new Person(matcher.group(1), matcher.group(2), matcher.group(3).charAt(0),Integer.parseInt( matcher.group(4)),DateFormatClass.getDateObj(matcher.group(5) + "-" + matcher.group(6) + "-" + matcher.group(7) + " " + matcher.group(8) + ":" + matcher.group(9)) , matcher.group(10)));
            }
        }
        return personSet;
    }


}

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class DateFormatClass {
    static final public java.text.DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy HH:mm");

    public static Date getDateObj(String dataString) throws ParseException {
        return dateFormat.parse(dataString);
    }
    public static String getDateStr(Date date){
        String s =  String.format("%1$td-%1$tm-%1$tY %1$tH:%1$tm", date);
        return s;//dateFormat.format(date);
    }
}

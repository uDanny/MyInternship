import org.junit.Test;
import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.text.ParseException;
import java.util.*;

public class FileWriterXMLTest {
    @Test
    public void writeFile() throws ParseException, IOException, ParserConfigurationException, SAXException {

        FileWriterClass fileWriterClass = new FileWriterXML("File.XML");

        Person p0 = new Person("Alex", "Ciobanu", 'M', 24, DateFormatClass.getDateObj("24-12-1993 18:24"), "bd.Dacia 16, ap.84");
        Person p1 = new Person("Ion", "Cibotaru", 'F', 26, DateFormatClass.getDateObj("4-12-1993 2:36"), "bd.Cuza-Voda 15, ap.32");
//        Person p2 = new Person("Valentin", "Istrati", 'F', 19, DateFormatClass.getDateObj("24-2-1993 12:14"), "bd.Stefan cel Mare 76, ap.4");
//        Person p3 = new Person("Tudor", "Turcanu", 'M', 31, DateFormatClass.getDateObj("24-12-1993 11:58"), "bd.Traian 1, ap.102");


        fileWriterClass.writeFile(new TreeSet<Person>( Arrays.asList(p0, p1)));
        //(<T...>) = T - genereic care primeste orice tip de parametri, ... - primeste n parametri

        //check set-ul de persoane este corect
        assert ((new TreeSet<Person>(Arrays.asList(p0, p1))).equals(new TreeSet<Person>(Arrays.asList(new Person("Alex", "Ciobanu", 'M', 24, DateFormatClass.getDateObj("24-12-1993 18:24"),
                "bd.Dacia 16, ap.84"), new Person("Ion", "Cibotaru", 'F', 26, DateFormatClass.getDateObj("4-12-1993 2:36"), "bd.Cuza-Voda 15, ap.32")))));

        //file-ul a fost creeat == dubios pentru ca daca nu, apare fileNotFound la etapa de mai jos
        assert (new File("File.XML").exists());

        //check datele din file sunt corecte ( == alt test )
        FileReaderClass fileReaderClass = new FileReaderXML("File.XML");
        assert (fileReaderClass.readFile().equals(new TreeSet<Person>(Arrays.asList(new Person("Alex", "Ciobanu", 'M', 24, DateFormatClass.getDateObj("24-12-1993 18:24"),
                "bd.Dacia 16, ap.84"), new Person("Ion", "Cibotaru", 'F', 26, DateFormatClass.getDateObj("4-12-1993 2:36"), "bd.Cuza-Voda 15, ap.32")))));
    }

    @Test
    public void testWriteExceptions() {
        try {
            writeFile();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (ParseException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (SAXException e) {
            e.printStackTrace();
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        }

    }
}


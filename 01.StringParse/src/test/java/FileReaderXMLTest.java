import org.apache.log4j.Logger;
import org.junit.Test;
import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import java.io.*;
import java.text.ParseException;
import java.util.Arrays;
import java.util.Set;
import java.util.TreeSet;

public class FileReaderXMLTest {
    @Test
    public void readFile() throws ParserConfigurationException, SAXException, ParseException, IOException {
        org.apache.log4j.Logger log = Logger.getLogger(this.getClass());

        FileReaderClass fileReaderClass = new FileReaderXML("File.XML");

        Set<Person> personSet = fileReaderClass.readFile();

        log.info(personSet.toString());

        //check daca fisierul exista
        assert (new File("File.XML").exists());

        //@mention
        //TODO: check text from file ??

        //check la introducerea in Set a obiectelor Person
        assert (personSet.equals(new TreeSet<Person>(Arrays.asList(new Person("Alex", "Ciobanu", 'M', 24, DateFormatClass.getDateObj("24-12-1993 18:24"),
                "bd.Dacia 16, ap.84"), new Person("Ion", "Cibotaru", 'F', 26, DateFormatClass.getDateObj("4-12-1993 2:36"), "bd.Cuza-Voda 15, ap.32")))));

    }

    @Test
    public void testReadExceptions()  {
        try {
            readFile();
        }catch (FileNotFoundException e){
            e.printStackTrace();
        }catch (IOException e) {
            e.printStackTrace();
        } catch (ParseException e) {
            e.printStackTrace();
        } catch (SAXException e) {
            e.printStackTrace();
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        }
    }
}
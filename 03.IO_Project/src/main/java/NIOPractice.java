import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.charset.StandardCharsets;

public class NIOPractice {
    static byte[] message = "aaadasdsa".getBytes(StandardCharsets.UTF_8); // Java 7+ only;

    private static final int ITERATIONS = 5;
    private static final double MEG = (Math.pow(1024, 2));
    private static final int RECORD_COUNT = 40000000;
    private static final String RECORD = "Help I am trapped in a fortune cookie factory\n";
    private static final int RECSIZE = RECORD.getBytes().length;

    public static void main(String[] args) throws IOException {

        NIOPractice obj = new NIOPractice();
        obj.write();
        obj.read();


    }
    public void read() throws IOException {
        //read
        FileInputStream fin = new FileInputStream( "writesomebytes.txt" );
        FileChannel fc = fin.getChannel();

        ByteBuffer buffer = ByteBuffer.allocate( 1024 );

        fc.read( buffer );


        buffer.flip();

        while (buffer.hasRemaining()) {
            System.out.print((char) buffer.get());
        }
        buffer.clear();



    }
    public void write() throws IOException {
        //write
        FileOutputStream fout = new FileOutputStream( "writesomebytes.txt" );
        FileChannel fc = fout.getChannel();

        ByteBuffer buffer = ByteBuffer.allocate( 1024 );

        for (int i=0; i<message.length; ++i) {
            buffer.put( message[i] );
        }
        buffer.flip();

        fc.write( buffer );
        buffer.clear();

    }
}

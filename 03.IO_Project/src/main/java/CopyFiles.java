import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.ArrayList;
import java.util.List;

public class CopyFiles {
    public static void main(String[] args) throws IOException {

        List<File> fileList = new ArrayList<>();

        fileList.add( new File("/home/dan/Documents/IT/temp/source/file1.txt"));
        fileList.add( new File("/home/dan/Documents/IT/temp/source/file2.txt"));
        fileList.add( new File("/home/dan/Documents/IT/temp/source/file3.txt"));

        for (File file : fileList){
            if (file.exists()){
                System.out.println("Fisierul " + file.getName() + " exista");
            }else {
                System.out.println("Fisierul " + file.getName() + " nu exista");
                    file.createNewFile();


            }

        }



       // String path11 = "/home/dan/Documents/IT/temp/destination/";


        Path path = (Paths.get(fileList.get(0).getAbsolutePath())).getParent().getParent();
        String path1 = new String(String.valueOf(path) + "/destination/");



        for (File file : fileList){
            Files.copy(file.toPath(),
            (new File(path1 + ( " Copiat! " + file.getName()))).toPath(),
                    StandardCopyOption.REPLACE_EXISTING);
        }


    }
}

package Architectoy;

import Architectoy.Read.ReaderFile;
import Architectoy.Read.ReadFormatFactory;
import Architectoy.Read.ReadableObject;
import Architectoy.Write.WriteFormatFactory;
import Architectoy.Write.WritableObject;
import Architectoy.Write.WriterFile;
import org.apache.log4j.Logger;

import java.util.Set;
import java.util.TreeSet;

public class Main {
    public static void main(String[] args) {

        //write

        WriteFormatFactory writeFormatFactory = new WriteFormatFactory();
        WritableObject writableObject = writeFormatFactory.getType("csv", new TreeSet<>());

        WriterFile writerFile = new WriterFile();
        writerFile.write(writableObject, "TestFile.csv");


        //read
        //Set<Person> personSet = Architectoy.Read.ReaderCSV(path, format)
        ReadFormatFactory readFormatFactory = new ReadFormatFactory();
        ReadableObject readableObject =  readFormatFactory.getType("csv");

        ReaderFile readerFile = new ReaderFile();
        Set<Person> personSet = readerFile.getSetPersons(readableObject, "path");

        //display
        Logger logger =Logger.getLogger("Main.class");
        for (Person person: personSet){
            //System.out.println(person.toString());
            logger.info(person.toString());
        }

    }
}

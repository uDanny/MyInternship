package Architectoy.Read;

import Architectoy.Person;

import java.util.Collection;
import java.util.Set;
import java.util.TreeSet;

public abstract class PersonSetReader implements ReadableObject {
    protected Set<Person> personSet;

    public PersonSetReader() {
        personSet = new TreeSet<>();
    }
    public PersonSetReader(Set<Person> personSet) {
        personSet = new TreeSet<>(personSet);
    }


    public Set<Person> getPersonSet() {
        return personSet;
    }

    @Override
    public Collection<?> getDataCollection() {
        return personSet;
    }
}

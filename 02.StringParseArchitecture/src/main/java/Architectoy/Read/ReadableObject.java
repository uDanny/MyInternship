package Architectoy.Read;

import java.util.Collection;

public interface ReadableObject {
    void readData();

    Collection<?> getDataCollection();
}

package Architectoy.Read;

public class ReadFormatFactory {

    public ReadableObject getType(String formatType) {
        if (formatType.equalsIgnoreCase("csv")){
            return new ReaderCSV();
        }else if (formatType.equalsIgnoreCase("xml")){
            return new ReaderXML();
        }
        return null;
    }
}

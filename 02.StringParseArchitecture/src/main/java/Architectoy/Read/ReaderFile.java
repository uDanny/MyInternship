package Architectoy.Read;

import Architectoy.Person;

import java.util.Collection;
import java.util.Set;
import java.util.TreeSet;

public class ReaderFile {

    public Set<Person> getSetPersons(ReadableObject readableObject, String path){
        //check if file exist
        //read file
        readableObject.readData();
        return new TreeSet<>((Collection<Person>) readableObject.getDataCollection());
    }
}

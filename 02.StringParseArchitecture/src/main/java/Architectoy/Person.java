package Architectoy;

import java.util.Date;

public class Person implements Comparable<Person>{


    private String name;
    private String surname;
    private char gender;
    private int age;
    private Date date;
    private String adress;

    public Person(String name, String surname, char gender, int age, Date date, String adress) {
        this.name = name;
        this.surname = surname;
        this.gender = gender;
        this.age = age;
        this.date = date;
        this.adress = adress;
    }

    public Person() {

    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public char getGender() {
        return gender;
    }

    public void setGender(char gender) {
        this.gender = gender;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getAdress() {
        return adress;
    }

    public void setAdress(String adress) {
        this.adress = adress;
    }

    @Override
    public String toString() {
        return String.format("%1$s, %2$s, %3$c, %4$d, %5$s, \"%6$s\" ",getName(), getSurname(), getGender(), getAge(), DateFormatClass.getDateStr(getDate()), getAdress());
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Person person = (Person) o;

        if (gender != person.gender) return false;
        if (age != person.age) return false;
        if (!name.equals(person.name)) return false;
        if (surname != null ? !surname.equals(person.surname) : person.surname != null) return false;
        if (date != null ? !date.equals(person.date) : person.date != null) return false;
        return adress != null ? adress.equals(person.adress) : person.adress == null;
    }

    @Override
    public int hashCode() {
        int result = name.hashCode();
        result = 31 * result + (surname != null ? surname.hashCode() : 0);
        result = 31 * result + (int) gender;
        result = 31 * result + age;
        result = 31 * result + (date != null ? date.hashCode() : 0);
        result = 31 * result + (adress != null ? adress.hashCode() : 0);
        return result;
    }

    public int compareTo(Person o) {
        if (this.age > (o.getAge())){
            return 1;
        }
        else if (this.age < (o.getAge())){
            return -1;
        }
        else return 0;
    }
}

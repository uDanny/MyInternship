package Architectoy.Write;

import Architectoy.Person;

import java.util.Set;
import java.util.TreeSet;

public abstract class PersonSetWriter implements WritableObject {
    private Set<Person> personSet;

    public PersonSetWriter(Set<Person> personSet) {
        personSet = new TreeSet<>(personSet);
    }

}


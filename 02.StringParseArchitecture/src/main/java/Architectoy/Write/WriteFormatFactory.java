package Architectoy.Write;

import Architectoy.Person;

import java.util.Set;

public class WriteFormatFactory {
    public WritableObject getType(String formatType, Set<Person> personSet) {
        if (formatType.equalsIgnoreCase("csv")){
            return new WriterCSV(personSet);
        }else if (formatType.equalsIgnoreCase("xml")){
            return new WriterXML(personSet);
        }
        return null;
    }
}
